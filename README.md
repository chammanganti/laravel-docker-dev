# laravel-docker-dev
> A simple dockerized development environment for Laravel.

## Why?
I just like it.

## Usage
```
make          - shows usage
make up       - builds and runs containers
make down     - stops containers
make sh-nginx - starts a shell to the nginx container
make sh-php   - starts a shell to the php container
make sh-mysql - starts a shell to the mysql container
make laravel  - initializes a new laravel project
```